<?php

include "ComparatorInterface.php";

class DefaultComparator implements ComparatorInterface
{
    public function gte(LinkNodeInterface $node1, LinkNodeInterface $node2): bool
    {
        return $node1->getData() >= $node2->getData();
    }

    public function lte(LinkNodeInterface $node1, LinkNodeInterface $node2): bool
    {
        return $node1->getData() <= $node2->getData();
    }

    public function lt(LinkNodeInterface $node1, LinkNodeInterface $node2): bool
    {
        return $node1->getData() < $node2->getData();
    }
    public function eq(LinkNodeInterface $node1, LinkNodeInterface $node2): bool
    {
        return $node1->getData() === $node2->getData();
    }

}
