<?php

interface ComparatorInterface
{
    public function gte(LinkNodeInterface $node1, LinkNodeInterface $node2): bool;

    public function lte(LinkNodeInterface $node1, LinkNodeInterface $node2): bool;

    public function lt(LinkNodeInterface $node1, LinkNodeInterface $node2): bool;

    public function eq(LinkNodeInterface $node1, LinkNodeInterface $node2): bool;
}
