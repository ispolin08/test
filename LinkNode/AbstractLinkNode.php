<?php

require_once "LinkNodeInterface.php";

abstract class AbstractLinkNode implements LinkNodeInterface
{
    protected $data;
    protected ?LinkNodeInterface $next;

    public function __construct()
    {
        $this->next = null;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getNext(): ?LinkNodeInterface
    {
        return $this->next;
    }

    public function setNext(?LinkNodeInterface $next): void
    {
        $this->next = $next;
    }

}
