<?php

require_once "AbstractLinkNode.php";

class LinkNodeString extends AbstractLinkNode
{
    public function __construct(string $data)
    {
        parent::__construct();
        $this->data = $data;
    }

}
