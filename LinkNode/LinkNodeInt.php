<?php

require_once "AbstractLinkNode.php";

class LinkNodeInt extends AbstractLinkNode
{
    public function __construct(int $data)
    {
        parent::__construct();
        $this->data = $data;
    }

}
