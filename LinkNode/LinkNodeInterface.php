<?php

interface LinkNodeInterface
{
    public function getData();

    public function getNext(): ?LinkNodeInterface;

    public function setNext(?LinkNodeInterface $next);
}
