<?php

class SingleLinkedList
{
    private ?LinkNodeInterface $head;
    private ?LinkNodeInterface $tail;
    private ?string $nodeType;
    private ComparatorInterface $comparator;

    public function __construct(ComparatorInterface $comparator)
    {
        $this->head = null;
        $this->tail = null;
        $this->nodeType = null;
        $this->comparator = $comparator;
    }

    public function add(LinkNodeInterface $node): void
    {
        if ($this->nodeType !== null && $this->nodeType !== get_class($node) ) {
            throw new RuntimeException('All nodes must have same class');
        }

        if ($this->head === null || $this->comparator->gte($this->head, $node)) {
            // Case A
            // When linked list empty
            // or new nodes are adding at beginning
            $node->setNext($this->head);
            if ($this->head === null) {
                $this->tail = $node;
            }
            $this->head = $node;
        } else {
            if ($this->comparator->lte($this->tail, $node)) {
                // Case B
                // Add node at the end
                $this->tail->setNext($node);
                // New last node
                $this->tail = $node;
            } else {
                $temp = $this->head;
                // Find position to add new node
                while ($temp->getNext() !== null &&
                    $this->comparator->lt($temp->getNext(), $node) ) {
                    // Visit to next node
                    $temp = $temp->getNext();
                }
                $node->setNext($temp->getNext());
                // Add new node
                $temp->setNext($node);
            }
        }

        if ($this->nodeType === null) {
            $this->nodeType = get_class($node);
        }
    }

    // Display linked list element
    // TODO should not be here
    public function display()
    {
        if ($this->head === null) {
            return;
        }
        $temp = $this->head;
        // iterating linked list elements
        while ($temp !== null) {
            echo " ". $temp->getData(). " →";
            // Visit to next node
            $temp = $temp->getNext();
        }
        echo " NULL\n";
    }


}
