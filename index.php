<?php

include "./LinkNode/LinkNodeInt.php";
include "./LinkNode/LinkNodeString.php";

include "./SingleLinkedList.php";
include "./Comparator/DefaultComparator.php";

$sll = new SingleLinkedList(
    new DefaultComparator()
);

// Construct sorted linked list
$sll->add(new LinkNodeInt(-5));
$sll->display();
$sll->add(new LinkNodeInt(15));
$sll->display();
$sll->add(new LinkNodeInt(7));
$sll->display();

try {
    $sll->add(new LinkNodeString('string'));
} catch (Exception $exception) {
    echo $exception->getMessage(). "\n\n";
}

$sll = new SingleLinkedList(
    new DefaultComparator()
);

$sll->add(new LinkNodeString('a'));
$sll->display();
$sll->add(new LinkNodeString('z'));
$sll->display();
$sll->add(new LinkNodeString('b'));
$sll->display();
